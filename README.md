# Nickthetait's remixed firmware for a LulzBot Mini

# Starting point
Latest stable version of firmware for the Mini (v1.1.0.11) from https://code.alephobjects.com/rMARLINfb1929d1f579658d6171ef8c2201bbf58b37c6bb

# What makes it different
A few small differences from the stock version:
* X and Y scaling set to 100% - this breaks backwards compatibility with existing G-Code files, recommend reslicing
* Different default Z offset

# Compilation
Tested on Ubuntu 17.04

* Get the C++ compiler `sudo apt-get install build-essential` https://help.ubuntu.com/community/InstallingCompilers
* Get another tool you will need `sudo apt install gcc-avr`
* Enter Marlin directory `cd <marlin-source-directory>/Marlin`
* Compile `make`
* Show the newly created .hex file with `ls -lh *.hex`

# Installation
* Flash the firmware onto your printer using either:
    * Cura's Machine -> Install custom firware...
    * Arduino IDE's File -> Upload

# Verification
* Connect to printer
* Send `M115` to check that the new firmware is running

# Z Offset
Defines the distance between print surface and nozzle, just after G29 completes. At last measure, the ideal setting for this number was -1.28 for MY printer, but yours will be slightly different. A number closer to Zero means a larger gap.

Here is how to adjust it:
* Get current offset `M851`
* Change offset (temporarily) `M851 Z-#.##`
* Store (permanently) `M500`
